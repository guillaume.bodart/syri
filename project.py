#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#######################################
      SyRi -- PROJECT MODULE
#######################################
Synthetic River tool 

This library is designed to extract and play with data from river simulation
"""

import numpy as np
import os, time, glob,io, matplotlib
from tkinter import Tk
from tkinter.filedialog import askopenfilename
from zipfile import ZipFile
from lspiv import velocities,transform
from tkinter import ttk,font
from tqdm import tqdm 
from scipy.interpolate import griddata
from PIL import Image 
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib as mpl
import plotlib 
import cv2 as cv
from tkinter.filedialog import askdirectory
import ctypes
from numpy.ctypeslib import ndpointer

if 'monStyle' in plt.style.available:
    plt.style.use('monStyle')
else:
    plt.style.use('ggplot')
    
s = ttk.Style()
s.theme_use('default')
default_font = font.nametofont("TkDefaultFont")
default_font.configure(family="Ubuntu", size=12)

class syriProject():
     """
     SyRi project class:
     
    Contains 
    ---------------------
        - manta [dataCl()]
            Contains the data extracted from the particle simulation. See information of dataCl for more details 
        
        - mantaFiltr [filteredCl()]
            manta data filtered. See information of filteredCl for more details
            
        - blender [dataCl()]
            Decline the manta data in blender units (meters and seconds)
            
        - blenderFiltr 
            blender data filtered. By default it is a copy of the mantaFiltr data converted to blender units 
            
        - interpolated [dataCl()]
            data interpolated on a grid, in blender units. These data will be used for comparison. 
            It uses the vel() class from lspiv library, module velocities. 
            
        - images [list of npArrayInt]
            list of images of the simulation 
            
        - param [syriparam()]
            Contains all the parameters used to transform data from mantaflow to blender units
        
    ---------------------------                      
    
    """
    
     def __init__(self,name = ""): 
         self.name          = name 
         self.manta         = []          # list of dataCl 
         self.mantaFiltr    = []          # list of filteredCl()
         self.blender       = []          # list of dataCl()
         self.blenderFiltr  = []          # list of filteredCl() 
         self.interpolated  = []          # list of vel()
         self.images        = []          # list of images 
         self.param         = syriparam() # class containing all infos           
         
     def readProject(self,
                     filename       = "",
                     keepinMem      = ["interpolated","blenderFiltr"],
                     mantaFilter    = "Nz > 0",
                     grid           = [],
                     isFudaaGrid    = False,
                     isMeterGrid    = True,
                     saveGrid       = False, 
                     interpolMethod = "nearest"):
         """
         Reads a syri project (.syri.zip). 
         
         Steps :    - 1/ Extract data from manta simulation 
                    - 2/ Filter the data based on mantaFilter
                    - 3/ Reads the reference files 
                    - 4/ Converts manta data to blender units 
                    - 5/ Interpolate velocities on grid 
                    (from file grid.dat or from data passed to the function)
                    - 6/ Import images 
                
         Parameters
         ----------
             - filename : [str], optional
                 Path to the syri project (.syri.lspiv). If nothing is entered then a GUI will be open to find the file.
                 The default is "".
             
             - keepinMem : [lstStr], optional
                 Determine which data will be kept in memory. As simulation files could be large it is better to save only important data. 
                 Expected values : 'manta','mantaFiltr','blender', 'blenderFiltr', 'interpolated'
                 Default value is ['blenderFiltr','interpolated']
                 
             - mantaFilter : [str], optional
                 Filter to be applied on the data extracted from manta simulation. 
                 Has to have the shape "[object] [operator] [value]"
                    --> e.g. "X > 0" or "Nz > 0" 
                 Default value is "Nz > 0" : normals on Z have to be positive
             
             - grid : [npArray], optional
                 Grid used to interpolate the data. Has to be a 2D array. 
                 Grid could be in meters or in pixels (set flag isMeterGrid). If grid coming from Fudaa-LSPIV flag isFudaaGrid has to be True.
                 Default value is []
                 
             - isFudaaGrid : [bool], optional
                 Flag used to inform if the grid comes from a Fudaa-LSPIV project. 
                 Fudaa-LSPIV grids are stored as [j,i] and therefore need to be corrected.
                 Default value is False
                 
             - isMeterGrid : [bool], optional
                 Flag used to inform about the grid units (pixels or meters)
                 Grids in pixels will be converted in meters based on the camera parameters founded in self.param.camera
                 Default value is True 
                 
             - saveGrid : [bool], optional
                 Not implemented yet... 
                 
             - interpolationMethod : [str], optional
                 Interpolation method (scipy.interpolate.griddata is used) 
                 Possible values : 'nearest', 'linear', 'cubic'
                 Default value is 'nearest'                
        
         Returns
         -------
             - errno [int]
                 - 0 - no errors 
                 - 1 - wrong file format 
                 - 2 - files are missing

         """
         # ---- INITIALISATION ----
         isExist = os.path.exists(filename) # Check if file exists 
        
         if (filename == "") or (isExist == False):
            # Open GUI to find file - tkinker lib is used 
            Tk().withdraw()     
            filename = askopenfilename(title = "Select Syri Project", filetypes = (("syri project","*.syri.zip"),("all files","*.*"))) # Get file path
        
         if (filename[-9:] != ".syri.zip" ):
            print("ERROR : file format doesn't match\n")
            return 1
         
         # Analyse keepinMem to determine which data will be kept in memory 
         dictKept = {
                    'manta'         : 'manta' in keepinMem,
                    'mantaFiltr'    : 'mantaFiltr' in keepinMem,
                    'blender'       : 'blender' in keepinMem,
                    'blenderFiltr'  : 'blenderFiltr' in keepinMem,
                    'interpolated'  : 'interpolated' in keepinMem,
                    }
        
         # ---- READING ZIP -----
         # Open project using ZipFile 
         with ZipFile(filename,'r') as foldzip:
            
            # Check if files exists ------
            dictFiles = { 
                    'data'          :foldzip.namelist().count("01_data/"),
                    'ref'           :foldzip.namelist().count("02_param/ref.dat"),
                    'ref_blender'   :foldzip.namelist().count("02_param/ref_blender.dat"),
                    'grid'          :foldzip.namelist().count("02_param/grid.dat"),
                    'images'        :foldzip.namelist().count("03_images/")
                    }
            # ---- 1-EXTRACT DATA
            if dictFiles['data']:
                # Get file names 
                lstzip = foldzip.namelist()
                lstzip = [s for s in lstzip if "01_data/mesh" in s]
                lstzip = np.sort(lstzip)
                # Get datas 
                for file in tqdm (lstzip,desc="Data extraction"): # tqdm is the lib generating for the toolbar 
                    buff    = foldzip.read(file).decode('utf-8')
                    # Do some checking 
                    if "blender" in buff.split("\n")[0]:
                        isblender = True #in this case data have been saved with saveDataTxt so they're already in blender units !
                        tmp     = dataCl()
                        tmp.readbuffer(buff)
                        self.blender.append(tmp)
                    else: 
                        isblender = False
                        # Extract data in manta list 
                        tmp     = dataCl()
                        tmp.readbuffer(buff)
                        self.manta.append(tmp)
                    
            # ---- 2-FILTER DATA 
                if not isblender:
                    for file in tqdm (self.manta,desc="Data filtering"): 
                        tmp     = filteredCl()
                        errno   = tmp.apply(file, mantaFilter)
                        if errno>0:
                            dictErr= {1:"Format of the filter is wrong",
                                       2:"No data"}
                            print("\nWARNING: {}. Can't apply filter".fomat(dictErr[errno]))
                        else:
                            self.mantaFiltr.append(tmp)
                    
            else: 
                print("\nERROR: No folder '01_dat/' in project. Can't go further.")
                return 2
                    
            # ---- 3-READ REFERENCES
            isRefReadOk = True
            
            if dictFiles["ref"]:
                buff = foldzip.read("02_param/ref.dat").decode("utf-8")
                lstbuff = buff.split("\n")
                # Create the syriparam() object
                tmp = syriparam()
                tmp.mantaResol      = float(lstbuff[1].split(":")[1])
                tmp.blt_timestp     = float(lstbuff[3].split(":")[1])
                tmp.frm_frmlgth     = float(lstbuff[4].split(":")[1])
                tmp.accelScale      = float(lstbuff[5].split(":")[1])
                tmp.timeScale       = float(lstbuff[7].split(":")[1])
            else:
                print("\nWARNING: ref.dat is missing. Can't transform data")
                isRefReadOk = False
                
            if dictFiles["ref_blender"]:
                buff = foldzip.read("02_param/ref_blender.dat").decode("utf-8")
                lstbuff = buff.split("\n")
                # Create the camera() object -> will be added to syryparam()
                tmpCam = transform.camera()
                tmpCam.position     = np.array(lstbuff[2].split(":")[1].split(","),dtype = float)
                tmpCam.scale        = float(lstbuff[3].split(":")[1])
                tmpCam.imagesize    = (int(lstbuff[4].split(":")[1].split(",")[0]),
                                       int(lstbuff[4].split(":")[1].split(",")[1]))
                tmpCam.fps          = float(lstbuff[5].split(":")[1])
                # Add camera() object to syriparam() object
                tmp.camera = tmpCam
                tmp.domainLocation  = np.array(lstbuff[7].split(":")[1].split(","),dtype = float)
                tmp.domainSize      = np.array(lstbuff[8].split(":")[1].split(","),dtype = float)
                
                self.param = tmp
            else: 
                print("\nWARNING: ref_blender.dat is missing. Can't transform data")
                isRefReadOk = False
             
            # ---- 4-TRANSFORM DATA                
            if isRefReadOk:
                # In case data extracted are not directly in blender units 
                if not isblender:
                    # Define lower coner of the domain
                    lwcnr = self.param.domainLocation - (0.5*self.param.domainSize)
                    for file in tqdm (self.manta,desc="Transform manta to blender"): 
                        tmp = dataCl() 
                        # Apply transform (divide position by upres factor)
                        tmp.pos         = (file.pos/file.upres) * self.param.mantaResol + lwcnr
                        tmp.vel         = file.vel / self.param.timeScale
                        tmp.gridSize    = (file.gridSize / file.upres) * self.param.mantaResol + lwcnr
                        tmp.normals     = file.normals
                        tmp.type        = file.type
                        self.blender.append(tmp)
                # Save memory space by deleting manta data
                if not dictKept['manta']:
                    print("\nINFO: manta data are delete")
                    self.manta = []    
                if not dictKept['mantaFiltr']:
                    print("\nINFO: mantaFiltr data are delete")
                    self.mantaFiltr = []
                    
                for file in tqdm (self.blender,desc="Data filtering"): 
                    tmp = filteredCl()
                    errno = tmp.apply(file, mantaFilter)
                    if errno>0:
                        dictErr= {1:"Format of the filter is wrong",
                                   2:"No data"}
                        print("\nWARNING: {}. Can't apply filter".fomat(dictErr[errno]))
                    else:
                        self.blenderFiltr.append(tmp) 
                        
                if not dictKept['blender']:
                    print("\nINFO: blender data are delete")
                    self.blender = []
                    
            # ---- 5-INTERPOLATE ON GRID
            isGridOk = True
            # Get grid informations 
            if grid == []:
                # No grid is given to the function -> look for file grid.dat 
                if dictFiles['grid']:
                    # read file 
                    buff = foldzip.read("02_param/grid.dat").decode("utf-8")
                    if len(buff)!=0:
                        data = np.asarray(buff.split(),dtype = int)
                        # Reshape array to get 2D array
                        data = np.reshape(data,(-1,2))
                        # Swap columns to get (i,j)
                        grid = np.array([data[:,1],data[:,0]]).T   
                        # Convert grid from image index to meter 
                        grid = image2space(grid, self.param.camera)   
                        # Check if everything is ok 
                        if np.asarray(grid).ndim == 0:
                            print("\nWARNING: grid is empty. Can't interplolate data")
                            isGridOk = False
                    else:
                        print("\nWARNING: grid.dat is empty. Can't interpolate data")
                        isGridOk = False
                else: 
                    print("\nWARNING: file grid.dat not found. Can't interpolate data")
                    isGridOk = False
            else:
                # A grid is given to the function and will be used for interpolation
                if isFudaaGrid:
                    # Need to swap column and inverse i ax. 
                    grid = np.array([grid[:,1],grid[:,0]]).T # swap column
                    # Convert grid from image index to meter 
                    grid = image2space(grid, self.param.camera)  
                    # Check if everything is ok 
                    if np.asarray(grid).ndim == 0:
                        print("\nWARNING: grid is empty. Can't interplolate data")
                        isGridOk = False
                elif not isMeterGrid:
                    # Convert grid from image index to meter 
                    grid = image2space(grid, self.param.camera)  
                    # Check if everything is ok 
                    if np.asarray(grid).ndim == 0:
                        print("\nWARNING: grid is empty. Can't interplolate data")
                        isGridOk = False                    
            # Use grid to interpolate data 
            if isGridOk: 
                for file in tqdm (self.blenderFiltr,desc="Interpolation on grid"): 
                    tmp         = dataCl()
                    tmp.pos     = grid
                    try:
                        tmp.vel     = griddata(file.pos[:,:2], file.vel[:,:2], grid, method=interpolMethod)
                    except ValueError as e:
                        print("\nERROR: Problems with values in blenderFiltr. \n" + str(e))
                        break
                    tmp.type    = interpolMethod # Keep track on the interpolation method
                    # Add to list
                    self.interpolated.append(tmp)
                    
                if not dictKept['blenderFiltr']:
                    print("\nINFO: blenderFiltr data are delete")
                    self.blenderFiltr = []
                    
                if not dictKept['interpolated']:
                    print("\nINFO: interpolated data are delete")
                    self.interpolated = []
                    
            # ---- 6-IMPORT IMAGES

            if dictFiles['images']:
                lstzip = foldzip.namelist()
                lstzip = [s for s in lstzip if ("03_images/") in s]
                lstzip = np.sort(lstzip)
                if len(lstzip)==1:
                    print("\nWARNING: folder '03/images/' is empty")
                for file in tqdm (lstzip[1:],desc="Images extraction"): # tqdm is the lib generating for the toolbar 
                    buff    = foldzip.read(file) 
                    rdBuff  = io.BytesIO(buff) # Create a ByteIO object that will be red by PIL lib
                    tmpImg  = Image.open(rdBuff)
                    tmp     = np.asarray(tmpImg) # Converts to numpy array 
                    self.images.append(tmp)
            else:
                print("\nWARNING: no folder '03/images/' found in archive")
          
     def saveDataTxt(self,
                    data2save = "blender_filtered",
                    filename = "",
                    folder = ""):
        """
        Use to save data in a mesh.txt file 
        
        Usefull to save only filtered mesh particles to reduce I/O time 

        Parameters
        ----------
            - data2save : [str], optional
                Determine which data will be saved . Possibles values : "manta_filtered", "blender_filtered", 
                The default is "blender_filtered".
            - filename : [str], optional
                filename, added after "mesh_" and before the ID and extension "XXXX.txt"
                The default is "".
            - folder : [str], optional
                Determine where the data will be plotted. If "" then a GUI will be open to find the files 
                The default is "".

        Returns
        -------
            - errno : 
                - 0 - no errors 
                - 1 - wrong format or no data

        """
        # ---- INIT 
        isExist = os.path.exists(folder) # Check if file exists 
        # Choose folder 
        if (folder == "") or (isExist == False):
            # Open GUI to find file - tkinker lib is used 
            Tk().withdraw()     
            folder = askdirectory(title = "Save mesh_XXX.txt files to") # Get file path
        
        if filename == "":
            filename = "mesh_"
        else:
            filename = "mesh_" + filename
            
        
        # ---- WRITE 
        if data2save == "blender_filtered":
            # checking 
            if self.blenderFiltr == []:
                print("\nERROR: No data in blenderFiltr...")
                return 1 
            # loop on all data
            i = 0
            for data in tqdm (self.blenderFiltr,desc="Saving files "):
                # open file 
                f = open(folder + "/"+ filename + "%04d.txt" % i,'w')
                # write header
                line = "o Mesh - blender - GridSize : ([" + str(data.gridSize[0]) + "," + str(data.gridSize[1]) + "," + str(data.gridSize[2]) + "])\n"
                f.write(line.replace(".", ",")) #ensure compatibility with reading process
                # write particle data 
                for k in range(len(data.vel)):
                    line = "p ([" +   str(data.pos[k,0])   + "," +  str(data.pos[k,1])    + "," +   str(data.pos[k,2])   + "])" +"n ([" + str(data.normals[k,0]) + "," + str(data.normals[k,1]) + "," + str(data.normals[k,2]) + "])" +" v ([" +   str(data.vel[k,0])   + "," +   str(data.vel[k,1])   + "," +   str(data.vel[k,2])   + "])\n"
                    line = "p ([%.6f,%.6f,%.6f])" % (data.pos[k,0],data.pos[k,1],data.pos[k,2]) +"n ([%.6f,%.6f,%.6f])" % (data.normals[k,0],data.normals[k,1],data.normals[k,2]) +" v ([%.6f,%.6f,%.6f])\n" % (data.vel[k,0],data.vel[k,1],data.vel[k,2])
                    f.write(line.replace(".", ","))
                i = i+1
                
            f.close()
            
        elif data2save == "manta_filtered":
            # checking 
            if self.mantaFiltr == []:
                print("\nERROR: No data in mantaFiltr...")
                return 1 
            # loop on all data
            i = 0
            for data in tqdm (self.blenderFiltr,desc="Saving files "):
                # open file 
                f = open(folder + "/"+ filename + "%04d.txt" % i,'w')
                # write header
                line = "o Mesh - manta - GridSize : ([" + str(data.gridSize[0]) + "," + str(data.gridSize[1]) + "," + str(data.gridSize[2]) + "])\n"
                f.write(line.replace(".", ",")) #ensure compatibility with reading process
                # write particle data 
                for k in range(len(data.vel)):
                    line = "p ([" +   str(data.pos[k,0])   + "," +  str(data.pos[k,1])    + "," +   str(data.pos[k,2])   + "])" +"n ([" + str(data.normals[k,0]) + "," + str(data.normals[k,1]) + "," + str(data.normals[k,2]) + "])" +" v ([" +   str(data.vel[k,0])   + "," +   str(data.vel[k,1])   + "," +   str(data.vel[k,2])   + "])\n"
                    line = "p ([%.6f,%.6f,%.6f])" % (data.pos[k,0],data.pos[k,1],data.pos[k,2]) +"n ([%.6f,%.6f,%.6f])" % (data.normals[k,0],data.normals[k,1],data.normals[k,2]) +" v ([%.6f,%.6f,%.6f])\n" % (data.vel[k,0],data.vel[k,1],data.vel[k,2])
                    f.write(line.replace(".", ","))
                i = i+1
        f.close()
 
                    
                    
class syriparam():
    """
    Describes the parameters of a SyRi project 
    
    Contains
    ------------------
        - mantaResol [float]
            Resolution used for manta simulation
            
        - blt_timestp [float]
            Blender time per timestep unit (timestep = mantaflow time unit)
            
        - frm_frmlgth [float]
            Frame per framelength (belongs to mantaflow)
            
        - accelScale [float]
            Acceleration scale from mantaflow to blender units 
            
        - timeScale [float]
            Time scale from mantaflow to blender units 
      
        - camera [camera()]
            Contains camera information. camera() class is defined in lspiv library under the transformation module
            
        - domainLocation [npArrayFloat]
            Position of the domain in blender scene (and blender units)
            
        - domainSize [npArrayFloat]
            Size of the domain in blender units 
    """
    def __init__(self):
        self.mantaResol     = -99
        self.blt_timestp    = -99
        self.frm_frmlgth    = -99
        self.accelScale     = -99
        self.timeScale      = -99
        self.camera         = transform.camera()
        self.domainLocation = []
        self.domainSize     = []
        

class dataCl():
    """
    SyRi - data class:
        
    Contains
    ---------------------
    
        - type [str]
            Where the data came from, typically "particles" or "mesh"
    
        - pos [npArrayFloat]
            X,Y,Z position of particles -- stored as [[index] ,[X,Y,Z]]  
            
        - vel [npArrayFloat]
            Vx,Vy,Vz velocities of particles -- stored as [[index] ,[Vx,Vy,Vz]]
            
        - normals [npArrayFloat]
            Nx,Ny,Nz normal vector of the surface (only for mesh)
        
    ---------------------
    """
    def __init__(self): 
         self.type      = "" 
         self.pos       = []
         self.vel       = []
         self.normals   = []
         self.gridSize  = []
         self.upres     = 0
         
    def read(self,filename = ""):
        """
        Read the file "filename" to extract data. 
        
        Parameters
        ----------
            - filename : [str]
                Path to the file. If filename = "" then a GUI will be opened to find the file
            
        Returns
        -------
            - errno : [int]
                - 0 - No errors
                - 1 - Format isn't ".txt" 
                - 2 - Empty file
        """
        # ---- INITIALISATION 

        isExist = os.path.exists(filename) # Check if file exists 
        
        if (filename == "") or (isExist == False):
            # Open GUI to find file - tkinker lib is used 
            Tk().withdraw()     
            filename = askopenfilename() # Get file path
        
        if (filename[-4:] != ".txt" ):
            print("ERROR : file format doesn't match\n")
            return 1
        
        # ---- READING
        with open(filename,'r') as file:
            # Put data in buffer
            buff = file.read().split("\n")
        # Check if file is empty
        if len(buff) == 0:
            print("ERROR : empty file")
            return 2
        # Get header info
        header          = buff[0].split(" ")
        self.dtype      = header[1].lower() # use .lower() to remove capital letters
        self.upres      = float(header[5])
        gline           = header[-1].split("[")[1].split("]")[0].split(",")
        self.gridSize   = np.array([gline[0]+"."+gline[1],gline[2]+"."+gline[3],gline[4]+"."+gline[5]],dtype=float)
        # Get data
        buffData        = buff[1:-1]
        # Check if file contains data
        if len(buffData) == 0:
            print("ERROR : empty file - no data")
            return 2 
        # Read data     
        tmpPos,tmpVel,tmpNormals = [],[],[]
        for line in buffData:
            # position 
            tmp = line.split("[")[1].split("]")[0].split(",") 
            if(tmp[0] == "-nan"): # Case of nan values 
                tmpPos.append([tmp[0],tmp[1],tmp[2]])
            else:        
                tmpPos.append([tmp[0] + "." + tmp[1], 
                               tmp[2] + "." + tmp[3], 
                               tmp[4] + "." + tmp[5]])
            # normals
            tmp = line.split("[")[2].split("]")[0].split(",")
            tmpNormals.append([tmp[0] + "." + tmp[1],
                               tmp[2] + "." + tmp[3],
                               tmp[4] + "." + tmp[5]])
            # velocity
            tmp = line.split("[")[3].split("]")[0].split(",")
            tmpVel.append([tmp[0] + "." + tmp[1],
                           tmp[2] + "." + tmp[3],
                           tmp[4] + "." + tmp[5]])
            
        self.pos     = np.array(tmpPos,dtype = float)
        self.vel     = np.array(tmpVel,dtype = float)
        self.normals = np.array(tmpNormals,dtype = float)
        return 0
         
    def readbuffer(self,buffer = ""):
        """
        Read directly the buffer to extract data. 
        
        Parameters
        ----------
            - buffer : [str]
                buffer containing the data to be extracted
            
        Returns
        -------
            - errno : [int]
                - 0 - No errors
                - 1 - Empty buffer 
                - 2 - No data in file
        """
        # ---- CHECKINGS 
        if (buffer == "" ):
            print("ERROR : empty buffer")
            return 1
        
        # ---- READING
        buff = buffer.split("\n")
        # Get header info
        header          = buff[0].split(" ")
        self.dtype      = header[1].lower() # use .lower() to remove capital letters
        if("upres" in header):
            self.upres      = float(header[5])
        else:
            self.upres      = 0.
        # Grid size (decimals denoted with coma)
        gline           = header[-1].split("[")[1].split("]")[0].split(",")
        self.gridSize   = np.array([gline[0]+"."+gline[1],gline[2]+"."+gline[3],gline[4]+"."+gline[5]],dtype=float)
        # Get data
        buffData        = buff[1:-1]
        # Check if file contains data
        if len(buffData) == 0:
            print("ERROR : no data in file")
            return 2 
        # Read data     
        tmpPos,tmpVel,tmpNormals = [],[],[]
        for line in buffData:
            # position 
            tmp = line.split("[")[1].split("]")[0].split(",") 
            if(tmp[0] == "-nan" or tmp[0] == "nan"): # Case of nan values 
                tmpPos.append([tmp[0],tmp[1],tmp[2]])
            else:        
                tmpPos.append([tmp[0] + "." + tmp[1], 
                               tmp[2] + "." + tmp[3], 
                               tmp[4] + "." + tmp[5]])
            # normals
            tmp = line.split("[")[2].split("]")[0].split(",")
            tmpNormals.append([tmp[0] + "." + tmp[1],
                               tmp[2] + "." + tmp[3],
                               tmp[4] + "." + tmp[5]])
            # Vitesse
            tmp = line.split("[")[3].split("]")[0].split(",")
            tmpVel.append([tmp[0] + "." + tmp[1],
                           tmp[2] + "." + tmp[3],
                           tmp[4] + "." + tmp[5]])
            
        self.pos     = np.array(tmpPos,dtype=float)
        self.vel     = np.array(tmpVel,dtype = float)
        self.normals = np.array(tmpNormals,dtype = float)
        return 0

    def arrow2D(self,
                ax          = None, 
                plot        = "velXY", 
                title       = "", 
                xlbl        = "", 
                ylbl        = "", 
                color       = "red",
                cmapData    = "norm",
                cmap        = None, 
                scale       = None,
                **param_dict):
        """
        Plot the data 

        Parameters
        ----------
            - ax [plt.ax], optional
                ax where the data will be ploted. If None then a new plot will be create 
                The default is None.
                
            - plot [str], optional
                Data to be ploted. Can be velocity (vel) or position (pos).
                Two last characters determined the axes of projection (X,Y,Z).
                The default is "velXY"
                
            - title [str], optional
                Title of the plot. 
                The default is ""
                
            - xlbl [str], optional
                X label of the plot. 
                The default value is ""
                
            - ylbl [str], optional
                Y label of the plot. 
                The default value is ""

                
            - color [str], optional
                Color of the plot. If cmap is given then cmap will be applied instead
                The default is "red".
            
            - cmapData [npArray] or [str], optional
                Data used for the cmap. It has to be a 1D array of same size as data  
                The default is "norm". In this case norm is automatically calculated 
                
            - cmap [str], optional
                cmap applied to the data. 
                The default is None.
            
            - scale [int], optional
                Scale value used for arrows. If None then scale is automatic
                The default is None.

        Returns
        -------
            - qv [ax.quiver or ax.scatter]
                Plot of the data
            - errno [int]
                Error number :
                    - 1 - input problem 
                

        """
        # ---- CHECK INPUTS ----
        # first check on size 
        if len(plot)!=5:
            print("\nERROR: wrong format of variable 'plot'. See help file for more information.")
            return 1
        # then check contents
        dictType    = {"vel":0,"pos":1}
        dictAxes    = {"X":0,"Y":1,"Z":2}
        mtype       = plot[:3]
        maxes       = plot[3:]
        if not mtype in dictType or not maxes[0] in dictAxes or not maxes[1] in dictAxes:
            print("\nERROR: wrong format of variable 'plot'. See help file for more information.")
            return 1
        # check axes  
        if ax is None: # Creates figure and ax
            fig = plt.figure()
            ax = fig.add_subplot(111)
            plt.axis("equal")
            ax.set_title(title)
            ax.set_xlabel(xlbl)
            ax.set_ylabel(ylbl)
        if type(ax) != matplotlib.axes._subplots.Subplot:
            print("\nERROR: wrong format of variable ax. Expected : matplotlib.axes._subplots.Subplot.")
            return 1
        if cmapData != "norm" and len(cmapData) != len(self.vel[:,0]):
            print("\nERROR : Size of cmapData doesn't match data.")
            return 1 
        # label of colorbar
        if "lblClbr" in param_dict.keys():
            lblClbr = param_dict["lblClbr"]
            del param_dict["lblClbr"]
        else:
            lblClbr = "Velocity [m/s]"
        # label of colorbar
        if "vmin" in param_dict.keys():
            vmin = param_dict["vmin"]
            del param_dict["vmin"]
        else:
            vmin = None
        # label of colorbar
        if "vmax" in param_dict.keys():
            vmax = param_dict["vmax"]
            del param_dict["vmax"]
        else:
            vmax = None
        
        # ---- PLOT ----
        if mtype == "vel":
            # Compute norm     
            norm = np.sqrt(self.vel[:,dictAxes[maxes[0]]]**2 + self.vel[:,dictAxes[maxes[1]]]**2)
            if cmap is None: 
                try:
                    qv = ax.quiver(self.pos[:,dictAxes[maxes[0]]],
                                   self.pos[:,dictAxes[maxes[1]]],
                                   self.vel[:,dictAxes[maxes[0]]],
                                   self.vel[:,dictAxes[maxes[1]]],
                                   color        = color,
                                   scale        = scale,
                                   scale_units  = "xy",
                                   **param_dict)
                except ValueError as e:
                    print("\nERROR : " + str(e))
                    return 2
                except TypeError as e:
                    print("\nERROR : " + str(e))
                    return 2
                except:
                    print("\nERROR: Problems while plotting data.")
                    return 2
            else:
                if cmapData == 'norm':
                    cmapData = norm
                try: 
                    qv = ax.quiver(self.pos[:,dictAxes[maxes[0]]],
                                    self.pos[:,dictAxes[maxes[1]]],
                                    self.vel[:,dictAxes[maxes[0]]],
                                    self.vel[:,dictAxes[maxes[1]]],
                                    cmapData,
                                    cmap            = cmap,
                                    scale           = scale,
                                    scale_units     = "xy",
                                    **param_dict)
                    
                    if vmin == None:
                        vmin = norm.min()
                    if vmax == None:
                        vmax = norm.min()
                    mapp = mpl.cm.ScalarMappable(mpl.colors.Normalize(vmin = vmin,vmax = vmax),cmap = cmap)
                    # Add colorbar 
                    plt.colorbar(mapp,ax=ax,label = lblClbr)
            
                except ValueError as e:
                    print("\nERROR : " + str(e))
                    return 2
                except TypeError as e:
                    print("\nERROR : " + str(e))
                    return 2
                except:
                    print("\nERROR: Problems while plotting data.")
                    return 2
        else:
            try:
                qv = ax.scatter(self.pos[:,dictAxes[maxes[0]]],
                                 self.pos[:,dictAxes[maxes[1]]],
                                 marker     = "+",
                                 s          = scale,
                                 color      = color,
                                 **param_dict)
            except ValueError as e:
                print("\nERROR : " + str(e))
                return 2
            except TypeError as e:
                print("\nERROR : " + str(e))
                return 2
            except:
                print("\nERROR: Problems while plotting data.")
                return 2
            
        return qv
    
    def plot(self,
             ax         = None,
             qty        = "norm", 
             method     = "hist",
             title      = "", 
             xlbl       = "", 
             ylbl       = "",
             **param_dict):
        """
        1D plot of data. Could be 'hist','qty=f(x)','qty=f(y)','y=f(qty)', 'x=f(qty)','sig=f(mean)' 

        Parameters
        ----------
            - ax [plt.ax], optional
                ax where the data will be ploted. If None then a new plot will be create 
                The default is None.
                
            - qty [str], optional
                Quantity to be plotted 
                Could be 'norm', 'angle', 'x', 'y'
                Default value is 'norm'
                
            - method [str], optional
                Type of plot 
                'hist','qty=f(x)','qty=f(y)','y=f(qty)', 'x=f(qty)','sig=f(mean)'
                Default value is 'hist'
                
            - title [str], optional
                Title of the plot. 
                The default is ""
                
            - xlbl [str], optional
                X label of the plot. 
                The default value is ""
                
            - ylbl [str], optional
                Y label of the plot. 
                The default value is ""
                
            - **param_dict : [dict], optionnal
                Extra parameters given to the plot function

            --- ADDITIONARY ARGUMENT FOR HIST : 

            - nbins [int] or [npArray] or [str] or [None], optional
                If an integer is given, bins + 1 bin edges are calculated and returned, consistent with numpy.histogram.
                If bins is a sequence, gives bin edges, including left edge of first bin and right edge of last bin. In this case, bins is returned unmodified.
                If a string is given it has to describe a strategy : 'auto', 'sturges', 'fd', 'doane', 'scott', 'rice' or 'sqrt'
                For more info see numpy.histogram.
                Default value is 'fd' 
                
            - range [tuple] or [None], optional
                The lower and upper range of the bins. If not provided, range is simply (a.min(), a.max()). Values outside the range are ignored. 
                The first element of the range must be less than or equal to the second. range affects the automatic bin computation as well. 
                While bin width is computed to be optimal based on the actual data within range, the bin count will fill the entire range including portions containing no data.
                Default is None
                
            - isdensity [bool], optional
                If False, the result will contain the number of samples in each bin. If True, the result is the value of the probability density function at the bin, normalized such that the integral over the range is 1. 
                Note that the sum of the histogram values will not be equal to 1 unless bins of unity width are chosen; it is not a probability mass function.
                Default is True
                
            - showHist [bool], optional
                Flag that indicates whether the histogram has to be plotted or not 
                Default is False 
            
            - showPdf [int], optional
                Flag that indicates whether the probability density function (pdf) has to be plotted or not 
                Default is True
            
            - showCdf [int], optional
                Flag that indicates whether the cumulative density function (cdf) has to be plotted or not 
                Default is False

        Returns
        -------
            - mplt [ax.hist or ax.plot]
                Plot of the data
            - errno [int]
                Error number :
                    - 1 - wrong input format 

        """
        # ---- Checking
        if not qty in ['norm','angle','x','y']:
            print("\nERROR: wrong format of variable 'qty'. See help file for more information.")
            return 1
        if not method in ['hist','qty=f(x)','qty=f(y)','y=f(qty)', 'x=f(qty)','sig=f(mean)']:
            print("\nERROR: wrong format of variable 'method'. See help file for more information.")
            return 1
        # check axes  
        if ax is None: # Creates figure and ax
            fig = plt.figure()
            ax = fig.add_subplot(111)
            ax.set_title(title)
            ax.set_xlabel(xlbl)
            ax.set_ylabel(ylbl)
        if type(ax) != matplotlib.axes._subplots.Subplot:
            print("\nERROR: wrong format of variable ax. Expected : matplotlib.axes._subplots.Subplot.")
            return 1
        # ---- Plot
        if method == "hist":
            if qty == "x":
                mplt = plotlib.hist(ax          = ax,
                                    data1       = self.vel[:,0],
                                    **param_dict)
            if qty == "y":
                mplt = plotlib.hist(ax          = ax,
                                    data1       = self.vel[:,1],
                                    **param_dict)
            if qty == "norm":
                norm = np.sqrt(self.vel[:,0]**2 + self.vel[:,1]**2)
                mplt = plotlib.hist(ax          = ax,
                                    data1       = norm,
                                    **param_dict)
            if qty == "angle":
                angle = np.arctan2(self.vel[:,1],self.vel[:,0])
                mplt = plotlib.hist(ax          = ax,
                                    data1       = angle,
                                    **param_dict)
                
        return mplt

    def copy(self):
        """
        Return a copy of the data

        Returns
        -------
           out - [data()]
               Copy of the data 

        """
        out = dataCl()
        if(len(self.pos)!=0):
            out.pos      = self.pos.copy()
            out.vel      = self.vel.copy()
            out.normals  = self.normals.copy()
            out.gridSize = self.gridSize.copy()
            out.upres    = self.upres
        return out
            
    def isempty(self):
        """
        Check is object is empty 

        Returns
        -------
            - out [bool]
                True if empty, False otherwise 

        """
        if(len(self.pos) == 0):
            return True
            
    def compare(self,
                compData,
                ax          = None,
                qty         = "norm",
                method      = "AE", 
                title       = "", 
                xlbl        = "", 
                ylbl        = "",
                **param_dict):
        """
        Draw plot to compare data to another dataCl object. 
        Quantity to be plotted could be : 'norm', 'angle', 'x', 'y'
        Method could be : 'dist', 'dist_rel', 'dist_abs',
                          'dist_rel_sort', 'dist_abs_sort',
                          'dist=f(r)', 'dist_hist','dist_abs_hist',
                          'dist_rel_hist','dist_rel_abs_hist','AE', 'AE_pix', 'AE_2D','AE_2D_pix', 'EE', 'EE_pix'

        Parameters
        ----------
            - compData : [dataCl()]
                DataCl object to be compared with 
            
            - ax : [plt.Axes], optional
                Ax use to plot the data. If None then a figure will be created.
                The default is None.
            
            - qty : [str], optional
                Quantity to be plotted. Expected values : 'norm', 'angle', 'x', 'y'
                The default is "norm".
            
            - method : [str], optional
                Method used to compare data. Expected values : 'dist', 'dist_rel', 'dist_abs', 'vv', 'dist_rel_sort', 'dist_abs_sort', 'AE', 'EE','dist=f(r)', 'dist_hist','dist_abs_hist','dist_rel_hist','dist_rel_abs_hist', 'AE', 'EE', 'EE_pix'
                The default is "AE".
            
            - hist_kws : [dict], optionnal
                arguments used for the histogram plot. 
            
            - **param_dict : [dict], optionnal
                Extra parameters given to the plot function

            --- ADDITIONARY ARGUMENT FOR HIST : 

            - nbins [int] or [npArray] or [str] or [None], optional
                If an integer is given, bins + 1 bin edges are calculated and returned, consistent with numpy.histogram.
                If bins is a sequence, gives bin edges, including left edge of first bin and right edge of last bin. In this case, bins is returned unmodified.
                If a string is given it has to describe a strategy : 'auto', 'sturges', 'fd', 'doane', 'scott', 'rice' or 'sqrt'
                For more info see numpy.histogram.
                Default value is 'fd' 
                
            - range [tuple] or [None], optional
                The lower and upper range of the bins. If not provided, range is simply (a.min(), a.max()). Values outside the range are ignored. 
                The first element of the range must be less than or equal to the second. range affects the automatic bin computation as well. 
                While bin width is computed to be optimal based on the actual data within range, the bin count will fill the entire range including portions containing no data.
                Default is None
                
            - isdensity [bool], optional
                If False, the result will contain the number of samples in each bin. If True, the result is the value of the probability density function at the bin, normalized such that the integral over the range is 1. 
                Note that the sum of the histogram values will not be equal to 1 unless bins of unity width are chosen; it is not a probability mass function.
                Default is True
                
            - showHist [bool], optional
                Flag that indicates whether the histogram has to be plotted or not 
                Default is False 
            
            - showPdf [int], optional
                Flag that indicates whether the probability density function (pdf) has to be plotted or not 
                Default is True
            
            - showCdf [int], optional
                Flag that indicates whether the cumulative density function (cdf) has to be plotted or not 
                Default is False
        
            --- ADDITIONARY ARGUMENT FOR HIST
            
            - legend [bool], optionnal
                Indicates if legend has to be plotted, mainly for histograms and statistics. 
                
             --- ADDITIONARY ARGUMENT FOR EE_PIX : 

            - camera [lspiv.transform.camera()], optional
                Camera data used to change from world space to image space and from velocity to displacement
                camera is needed for EE_pix /!\ 
        Returns
        -------
            - errno : [int]
                Error ID : - 1 - Wrong input format
                

        """
         # ---- CHECK INPUTS ----
        # first check on inputs 
        if not qty in ['norm','angle','x','y']:
            print("\nERROR: wrong format of variable 'qty'. See help file for more information.")
            return 1
        if not method in ['dist','dist_rel','dist_abs','dist_rel_abs','vv','dist_sort','dist_rel_sort','dist_abs_sort','dist_rel_abs_sort','dist=f(r)', 'dist_hist','dist_abs_hist','dist_rel_hist','dist_rel_abs_hist', 'AE', 'EE','EE_pix','AE_pix', 'AE_2D','AE_2D_pix']:
            print("\nERROR: wrong format of variable 'method'. See help file for more information.")
            return 1
        if type(compData)!= type(self):
            print("\nERROR: wrong format of variable 'compData'. Expected : syri.project.dataCl.")
            return 1
        # check axes  
        if ax is None: # Creates figure and ax
            fig = plt.figure()
            ax = fig.add_subplot(111)
        if type(ax) != matplotlib.axes._subplots.Subplot:
            print("\nERROR: wrong format of variable ax. Expected : matplotlib.axes._subplots.Subplot.")
            return 1
        # Extract camera from kwargs (needed for EE_pix only!)
        if "camera" in param_dict.keys():
            camera = param_dict["camera"]
            del param_dict["camera"]
        else:
            if method in ["EE_pix","AE_pix","AE_2D_pix"]:
                print("\nERROR: camera data is mandatory for this method ! Please add it to kwargs")
                return 1
            
        # ---- PLOT ----
        dictPlotFct = {"dist"               : plotlib.dist,
                       "dist_abs"           : plotlib.dist_abs,
                       "dist_rel"           : plotlib.dist_rel,
                       "dist_rel_abs"       : plotlib.dist_rel_abs,
                       "dist_sort"          : plotlib.dist_sort,
                       "dist_abs_sort"      : plotlib.dist_abs_sort,
                       "dist_rel_sort"      : plotlib.dist_rel_sort,
                       "dist_rel_abs_sort"  : plotlib.dist_rel_abs_sort,
                       "dist_hist"          : plotlib.dist_hist,
                       "dist_abs_hist"      : plotlib.dist_abs_hist,
                       "dist_rel_hist"      : plotlib.dist_rel_hist,
                       "dist_rel_abs_hist"  : plotlib.dist_rel_abs_hist,
                       "AE"                 : plotlib.AE,
                       "EE"                 : plotlib.EE,
                       "AE_2D"              : plotlib.AE_2D
               }
        
        # Special needs for EE_pix -> convertion from vel in meters to disp in pixels
        if method in ["EE_pix","AE_pix","AE_2D_pix"]:
            # Create new dataCl for displacement 
            selfDsp         = self.copy()
            compDsp         = compData.copy()
            # Converts now to pixel displacement 
            try:
                selfDsp.vel     = camera.velSpace2dispImage(selfDsp.vel)
                compDsp.vel     = camera.velSpace2dispImage(compDsp.vel)
            except Exception as e:
                print("\nERROR: Problem while changing from velocity to pixels displacement... \n Exception: " + str(e))
                return 1
            # Plot 
            meth = method[:-4]
            mplt = dictPlotFct[meth](ax,selfDsp.vel,compDsp.vel,**param_dict)
        # Plot functions that requires extra data (where dictPlotFct can't be used)
        elif method in ["AE","EE","AE_2D"]:
            # these methods compares directly the vectors so no need to specify qty      
            mplt = dictPlotFct[method](ax,self.vel,compData.vel,**param_dict)
        else: 
            if   qty == "x":
                mplt = dictPlotFct[method](ax,self.vel[:,0],compData.vel[:,0],**param_dict) 
            elif qty == "y":
                mplt = dictPlotFct[method](ax,self.vel[:,1],compData.vel[:,1],**param_dict) 
            elif qty == "norm": 
                # Calculate norm
                norme1 = np.sqrt(self.vel[:,0]**2 + self.vel[:,1]**2)
                norme2 = np.sqrt(compData.vel[:,0]**2 + compData.vel[:,1]**2)
                # plot
                mplt = dictPlotFct[method](ax,norme1,norme2,**param_dict)
            elif qty == "angle":
                # Calculate angle
                angle1 = np.arctan2(self.vel[:,1],self.vel[:,0])
                angle2 = np.arctan2(compData.vel[:,1],compData.vel[:,0])
                angle1 = np.rad2deg(angle1)
                angle2 = np.rad2deg(angle2)
                # plot
                mplt = dictPlotFct[method](ax,angle1,angle2,**param_dict)
            else: 
                print("\nERROR: wrong format for qty.")
                return 1
        # Add info on axes, if given to the function
        if (title != ""):
            ax.set_title(title)
        if (xlbl != ""):
            ax.set_xlabel(xlbl)
        if (ylbl != ""):
            ax.set_ylabel(ylbl)
            
        return mplt
        
    def compareArrow(self
                     ,compData,
                     ax          = None,
                     method      = "EE", 
                     **param_dict):
        """
        Compare vector fields. Several methods can be used : 
            - 'EE' endpoint error : difference between the vectors
        
        'self' is supposed to be the reference and compData the measurement
            
        Parameters
        ----------
            - compData : [dataCl()] 
                Data to be compared. Must be a dataCl() 
            - ax : [plt.Axes()], optional
                ax where the data will be plotted. If None a new figure will be created
                The default is None.
            - method : [str], optional
                method used for the plot. can be 'EE'
                The default is "EE".
            - **param_dict : kwargs
                kwargs passed to the function.

        Returns
        -------
            - errno : 
                - 0 - No errors 
                - 1 - File format 
                - 2 - Positions aren't equals 
                - 3 - Error while plotting

        """
         # ---- CHECK INPUTS ----
        # first check on inputs 
        if not method in ['EE','EE_pix','EE_%']:
            print("\nERROR: wrong format of variable 'method'. See help file for more information.")
            return 1
        if type(compData)!= type(self):
            print("\nERROR: wrong format of variable 'compData'. Expected : syri.project.dataCl.")
            return 1
        # check axes  
        if ax is None: # Creates figure and ax
            fig = plt.figure()
            ax = fig.add_subplot(111)
        if type(ax) != matplotlib.axes._subplots.Subplot:
            print("\nERROR: wrong format of variable ax. Expected : matplotlib.axes._subplots.Subplot.")
            return 1
        # check that positions of vectors or the same 
        if sum(sum(compData.pos - self.pos)) != 0.: 
            print("\nERROR: positions of the data are not the same")
            return 2
        
        # label of colorbar
        if "lblClbr" in param_dict.keys():
            lblClbr = param_dict["lblClbr"]
            del param_dict["lblClbr"]
        else:
            if method in ["EE"]:
                lblClbr = "Velocity difference [m/s]"
            elif method in ["EE_pix"]:
                lblClbr = "Displacement difference [pix]"
            elif method in ["EE_%"]:
                lblClbr = "Velocity difference [\%]"
                
         # scale for vectors 
        if "scale" in param_dict.keys():
            scale = param_dict["scale"]
            del param_dict["scale"]
        else:
            scale = None
                
        # Camera (MANDATORY for EE_pix)
        if "camera" in param_dict.keys():
            camera = param_dict["camera"]
            del param_dict["camera"]
        else: 
            if method in ["EE_pix"]:
                print("\nERROR: No camera info passed to the function. Can't convert velocity")
                return 1
            
        # ---- PLOT 
        if method == "EE":
            # compute diff
            ee = compData.vel - self.vel 
            # Compute norm 
            norm = np.sqrt(ee[:,0]**2 + ee[:,1]**2)
            try: 
                qv = ax.quiver(self.pos[:,0],
                               self.pos[:,1],
                               ee[:,0],
                               ee[:,1],
                               norm,
                               scale          = scale,
                               cmap           = "magma",
                               scale_units    = "xy",
                               **param_dict)
                mapp = mpl.cm.ScalarMappable(mpl.colors.Normalize(vmin = norm[np.isfinite(norm)].min(),vmax = norm[np.isfinite(norm)].max()),cmap = "magma")
                # Add colorbar 
                plt.colorbar(mapp,ax=ax,label = lblClbr)
            except ValueError as e:
                print("\nERROR : " + str(e))
                return 3
            except TypeError as e:
                print("\nERROR : " + str(e))
                return 3
            except Exception as e:
                print("\nERROR: Problems while plotting data. \nException : " + str(e))
                return 3
            
        elif method == "EE_pix":
            # Convert velocity to displacement in pixels 
            selfDsp         = self.copy()
            compDsp         = compData.copy()
            # Converts now to pixel displacement 
            try:
                selfDsp.vel     = camera.velSpace2dispImage(selfDsp.vel)
                compDsp.vel     = camera.velSpace2dispImage(compDsp.vel)
            except Exception as e:
                print("\nERROR: Problem while changing from velocity to pixels displacement... \n Exception: " + str(e))
                return 1
            # compute diff
            ee = compDsp.vel - selfDsp.vel 
            # Compute norm 
            norm = np.sqrt(ee[:,0]**2 + ee[:,1]**2)
            try: 
                qv = ax.quiver(self.pos[:,0],
                               self.pos[:,1],
                               ee[:,1], # As displacement are stored [i,j] and j -> x
                               ee[:,0],
                               norm,
                               scale           = scale,  
                               cmap            = "magma",
                               scale_units     = "xy",
                               **param_dict)
                mapp = mpl.cm.ScalarMappable(mpl.colors.Normalize(vmin = norm[np.isfinite(norm)].min(),vmax = norm[np.isfinite(norm)].max()),cmap = "magma")
                # Add colorbar 
                plt.colorbar(mapp,ax=ax,label = lblClbr)
            except ValueError as e:
                print("\nERROR : " + str(e))
                return 3
            except TypeError as e:
                print("\nERROR : " + str(e))
                return 3
            except:
                print("\nERROR: Problems while plotting data.")
                return 3
            
        elif method == "EE_%":
            # compute diff
            ee = (compData.vel - self.vel)
            # Compute norm 
            normRef = np.sqrt(self.vel[:,0]**2 + self.vel[:,1]**2)
            norm    = np.sqrt(ee[:,0]**2 + ee[:,1]**2)
            normRel = (norm/normRef)*100
            try: 
                qv = ax.quiver(self.pos[:,0],
                               self.pos[:,1],
                               ee[:,0],
                               ee[:,1],
                               normRel,
                               scale           = scale,  
                               cmap            = "magma",
                               scale_units     = "xy",
                               **param_dict)
                mapp = mpl.cm.ScalarMappable(mpl.colors.Normalize(vmin = normRel[np.isfinite(normRel)].min(),vmax = normRel[np.isfinite(normRel)].max()),cmap = "magma")
                # Add colorbar 
                plt.colorbar(mapp,ax=ax,label = lblClbr)
            except ValueError as e:
                print("\nERROR : " + str(e))
                return 3
            except TypeError as e:
                print("\nERROR : " + str(e))
                return 3
            except:
                print("\nERROR: Problems while plotting data.")
                return 3
        return qv 
    
    def scatter(self,
                compData,
                ax              = None,
                qty             = "norm",
                scatter_kws     = {"alpha"  : 0.3,
                                   "s"      :15},
                line_kws        = {"color"  :"black",
                                   "ls"     :"-",
                                   "lw"     :0.5},
                robust          = True,
                fit_reg         = True,
                cmap_data       = None,
                cmap_kws        = {"alpha"      : 0.6,
                                   "linewidths"  : 0.2,
                                   "vmin"       : None,
                                   "vmax"       : None,
                                   "cmap"       : "magma",
                                   "label"      : ""}):
        """
        Draw scatter plot to compare data to another dataCl object. 
        Quantity to be plotted could be : 'norm', 'angle', 'x', 'y'
        This function uses a linear regression tool (seaborn) to display linear fit and confidence intervals. 
        

        Parameters
        ----------
            - compData : [dataCl()]
                DataCl object to be compared with 
            
            - ax : [plt.Axes], optional
                Ax use to plot the data. If None then a figure will be created.
                The default is None.
            
            - qty : [str], optional
                Quantity to be plotted. Expected values : 'norm', 'angle', 'x', 'y'
                The default is "norm".
            
            - scatter_kws : [dict], optional
                dict of kwargs passed to the plot function. This one is used for the scatter plot. 
            
            - line_kws : [dict], optional
                dict of kwargs passed to the plot function. This one is used for the line plot (linear fit). 
            
            - robuts : [bool], optional
                If True then outliers are discard from linear fitting process. 
                
            - fit_reg : [bool], optional
                If False only the scatter plot is drawn (no linear fit)
            
            - cmap_data : [npArray], optionnal
                Data used to apply cmap on scatter. Must be same size of data. 
                If None a normal scatter plot with single color will be ploted. 
                Default value is None
               
            - cmap_kws : [dict], optionnal
                Dictionnary containing the argument used for the color mapped scatter.
                "vmin" and "vmax" express the range of the color map. If these values are None they'll be initialized with max and min of cmap_data
                

        Returns
        -------
            - errno : [int]
                Error ID : - 1 - Wrong input format
                

        """
         # ---- CHECK INPUTS ----
        # first check on inputs 
        if not qty in ['norm','angle','x','y']:
            print("\nERROR: wrong format of variable 'qty'. See help file for more information.")
            return 1
        if type(compData)!= type(self):
            print("\nERROR: wrong format of variable 'compData'. Expected : syri.project.dataCl.")
            return 1
        # check axes  
        if ax is None: # Creates figure and ax
            fig = plt.figure()
            ax = fig.add_subplot(111)
        if type(ax) != matplotlib.axes._subplots.Subplot:
            print("\nERROR: wrong format of variable ax. Expected : matplotlib.axes._subplots.Subplot.")
            return 1
        # ---- PLOT ----
        if qty == "x":
            
            data1 = self.vel[:,0]
            data2 = compData.vel[:,0]
            
            # Determine is cmap is used or not. 
            if (cmap_data is None):
                
                mplt = plotlib.vv(ax            = ax, 
                                  data1         = data1,
                                  data2         = data2,
                                  scatter_kws   = scatter_kws,
                                  line_kws      = line_kws,
                                  robust        = robust,
                                  fit_reg       = fit_reg)
                plt.axis("equal")
                
            else: 
            # If cmap is applied then scatterplot from seaborn is hidden and another scatter is plotted with the cmap args
                scatter_kws["visible"]=False # Add entry in scatter_kws dict to hide scatter plot
                # Plot the searborn data (fit_reg)
                mplt = plotlib.vv(ax            = ax, 
                                  data1         = data1,
                                  data2         = data2,
                                  scatter_kws   = scatter_kws,
                                  line_kws      = line_kws,
                                  robust        = robust,
                                  fit_reg       = fit_reg)
                plt.axis("equal")
                # Add cmapped scatter                
                msct = ax.scatter(data1,data2,
                                  c            = cmap_data, 
                                  cmap         = cmap_kws["cmap"],
                                  alpha        = cmap_kws["alpha"],
                                  vmin         = cmap_kws["vmin"], 
                                  vmax         = cmap_kws["vmax"], 
                                  linewidths   = cmap_kws["linewidths"])
                
                # Create colorbar entry
                mapp = mpl.cm.ScalarMappable(mpl.colors.Normalize(vmin = np.min(msct._A),
                                                                  vmax = np.max(msct._A)),
                                                                  cmap = cmap_kws["cmap"])
                # Add colorbar 
                ax.colorbar(mapp,ax=ax,label = cmap_kws["label"])
            
        elif qty == "y":
            
            data1 = self.vel[:,1]
            data2 = compData.vel[:,1]
            
            # Determine is cmap is used or not. 
            if cmap_data is None:
                
                mplt = plotlib.vv(ax            = ax, 
                                  data1         = data1,
                                  data2         = data2,
                                  scatter_kws   = scatter_kws,
                                  line_kws      = line_kws,
                                  robust        = robust,
                                  fit_reg       = fit_reg)
                plt.axis("equal")
                
            else: 
            # If cmap is applied then scatterplot from seaborn is hidden and another scatter is plotted with the cmap args
                scatter_kws["visible"]=False # Add entry in scatter_kws dict to hide scatter plot
                # Plot the searborn data (fit_reg)
                mplt = plotlib.vv(ax            = ax, 
                                  data1         = data1,
                                  data2         = data2,
                                  scatter_kws   = scatter_kws,
                                  line_kws      = line_kws,
                                  robust        = robust,
                                  fit_reg       = fit_reg)
                plt.axis("equal")
                # Add cmapped scatter                
                msct = ax.scatter(data1,data2,
                                  c            = cmap_data, 
                                  cmap         = cmap_kws["cmap"],
                                  alpha        = cmap_kws["alpha"],
                                  vmin         = cmap_kws["vmin"], 
                                  vmax         = cmap_kws["vmax"], 
                                  linewidths   = cmap_kws["linewidths"])
                
                # Create colorbar entry
                mapp = mpl.cm.ScalarMappable(mpl.colors.Normalize(vmin = np.min(msct._A),
                                                                  vmax = np.max(msct._A)),
                                                                  cmap = cmap_kws["cmap"])
                # Add colorbar 
                ax.colorbar(mapp,ax=ax,label = cmap_kws["label"])
            
        elif qty == "norm": 
            # Calculate norm
            data1 = np.sqrt(self.vel[:,0]**2 + self.vel[:,1]**2)
            data2 = np.sqrt(compData.vel[:,0]**2 + compData.vel[:,1]**2)
  
            # Determine is cmap is used or not. 
            if cmap_data is None:
                
                mplt = plotlib.vv(ax            = ax, 
                                  data1         = data1,
                                  data2         = data2,
                                  scatter_kws   = scatter_kws,
                                  line_kws      = line_kws,
                                  robust        = robust,
                                  fit_reg       = fit_reg)
                plt.axis("equal")
                
            else: 
            # If cmap is applied then scatterplot from seaborn is hidden and another scatter is plotted with the cmap args
                scatter_kws["visible"]=False # Add entry in scatter_kws dict to hide scatter plot
                # Plot the searborn data (fit_reg)
                mplt = plotlib.vv(ax            = ax, 
                                  data1         = data1,
                                  data2         = data2,
                                  scatter_kws   = scatter_kws,
                                  line_kws      = line_kws,
                                  robust        = robust,
                                  fit_reg       = fit_reg)
                plt.axis("equal")
                # Add cmapped scatter                
                msct = ax.scatter(data1,data2,
                                  c            = cmap_data, 
                                  cmap         = cmap_kws["cmap"],
                                  alpha        = cmap_kws["alpha"],
                                  vmin         = cmap_kws["vmin"], 
                                  vmax         = cmap_kws["vmax"], 
                                  linewidths   = cmap_kws["linewidths"])
                
                # Create colorbar entry
                mapp = mpl.cm.ScalarMappable(mpl.colors.Normalize(vmin = np.min(msct._A),
                                                                  vmax = np.max(msct._A)),
                                                                  cmap = cmap_kws["cmap"])
                # Add colorbar 
                ax.colorbar(mapp,ax=ax,label = cmap_kws["label"])
            
        elif qty == "angle":
            # Calculate angle
            data1 = np.arctan2(self.vel[:,1],self.vel[:,0])
            data2 = np.arctan2(compData.vel[:,1],compData.vel[:,0])
            data1 = np.rad2deg(data1) +90
            data2 = np.rad2deg(data2) +90
            # Determine is cmap is used or not. 
            if cmap_data is None:
                
                mplt = plotlib.vv(ax            = ax, 
                                  data1         = data1,
                                  data2         = data2,
                                  scatter_kws   = scatter_kws,
                                  line_kws      = line_kws,
                                  robust        = robust,
                                  fit_reg       = fit_reg)
                plt.axis("equal")
                
            else: 
            # If cmap is applied then scatterplot from seaborn is hidden and another scatter is plotted with the cmap args
                scatter_kws["visible"]=False # Add entry in scatter_kws dict to hide scatter plot
                # Plot the searborn data (fit_reg)
                mplt = plotlib.vv(ax            = ax, 
                                  data1         = data1,
                                  data2         = data2,
                                  scatter_kws   = scatter_kws,
                                  line_kws      = line_kws,
                                  robust        = robust,
                                  fit_reg       = fit_reg)
                plt.axis("equal")
                # Add cmapped scatter                
                msct = ax.scatter(data1,data2,
                                  c            = cmap_data, 
                                  cmap         = cmap_kws["cmap"],
                                  alpha        = cmap_kws["alpha"],
                                  vmin         = cmap_kws["vmin"], 
                                  vmax         = cmap_kws["vmax"], 
                                  linewidths   = cmap_kws["linewidths"])
                
                # Create colorbar entry
                mapp = mpl.cm.ScalarMappable(mpl.colors.Normalize(vmin = np.min(msct._A),
                                                                  vmax = np.max(msct._A)),
                                                                  cmap = cmap_kws["cmap"])
                # Add colorbar 
                ax.colorbar(mapp,ax=ax,label = cmap_kws["label"])
            
        else: 
            print("\nERROR: wrong format for qty.")
            return 1
        
        
        
        return mplt
        
    def applyFlspivFilter(self,Flspiv_data):
        """
        Correct velocity data of object base on Fudaa-LSPIV filtered velocities : 
            Set vel to nan when Fudaa-LSPIV velocity == 0 
        Flspiv_data could be either syri.project.dataCl or lspiv.velocities.vel 
        """
        # ---- Checkings
        if type(Flspiv_data) == type(self):
            case = 1
        elif type(Flspiv_data) == velocities.vel:
            case = 2
        else:
            print("\nERROR : Wrong format for Flspiv_data")
            
        # ---- Apply filtering 
        if case == 1:
            self.vel[Flspiv_data.vel == 0]          = np.nan
            Flspiv_data.vel[Flspiv_data.vel == 0]   = np.nan
        elif case ==2:
            isFiltre =  np.logical_and(Flspiv_data.vx == 0,Flspiv_data.vy == 0)
            self.vel[isFiltre]              = np.nan
            Flspiv_data.vx[isFiltre]        = np.nan
            Flspiv_data.vy[isFiltre]        = np.nan
            
        return   
        
    def applyRefFilter(self):
        """
        Filter errors of interpolation on the reference motion field. 
        Outliers are detected with simple tolerance of mean+4*sigma on magnitude of vectors 
        
        Returns
        -------
        None.

        """
        # Compute magnitude 
        mag = np.sqrt(self.vel[:,1]**2+self.vel[:,0]**2)
        
        # Sort magnitude and compute gradient 
        magSort     = np.sort(mag)
        diffMagSort = magSort[1:] - magSort[:-1]
        diffMagSort = diffMagSort[np.isfinite(diffMagSort)] # Remove nan values
        # Compute threshold based on mean and std
        mean        = np.mean(diffMagSort)
        std         = np.std(diffMagSort)
        thres       = mean + 4*std
        
        # init values
        i = 0 
        valFiltr = []
        # Check from bottom # Check from bottom (values that are to low regarding the global distribution)
        while diffMagSort[i] > thres and i <len(diffMagSort):
            valFiltr.append(i)
            i = i+1
        # init iter obj
        i = len(diffMagSort)-1
        # Check from top (values that are to high regarding the global distribution)
        while diffMagSort[i] > thres and i >=0:
            valFiltr.append(i+1) #+1 because we use the gradient table to find the value from init table
            i = i-1 
            
        # Find values of magnitude to be removed  
        mag2filtr = magSort[valFiltr]
        # find index
        for val in mag2filtr:
            ind = np.where(mag == val)
            self.vel[ind,:] = np.nan
            
        return
    
    def image(self,shapeGrid,ax = None,fig = None, qty = "x",cmap = "magma",vmin = None, vmax = None,**param_dict):
        """
        Represent data on using imshow function. Datas need to be reshaped according to shapeGrid. 
        WARNING : works only for interpolated data (regular grid).
        Qty could be either 'x','y','norm','angle'

        Parameters
        ----------
         - shapeGrid : [tuple]
            shape of the data grid.
         - qty : [str], optional
            Data to be plotted on the image. Could be 'x','y','norm','angle' 
            The default is "x".

        Returns
        -------
         - errno [int]
             Error ID : - 1 - wrong input format 
         - plt 
             imshow plot 

        """
        # ---- Checkings 
        if shapeGrid[0] * shapeGrid[1] != len(self.vel[:,0]):
            print("\nERROR : shapeGrid size doesn't match size of object.")
            return 1
        if not qty in ['norm','angle','x','y',"xy"]:
            print("\nERROR: wrong format of variable 'qty'. See help file for more information.")
            return 1
        # check axes  
        if ax is None: # Creates figure and ax
            fig = plt.figure()
            ax = fig.add_subplot(111)
        if type(ax) != matplotlib.axes._subplots.Subplot:
            print("\nERROR: wrong format of variable ax. Expected : matplotlib.axes._subplots.Subplot.")
            return 1
        
        # ---- Arraning data 
        if qty == "x":
            data    = np.reshape(self.vel[:,0],shapeGrid)
            lblClbr = "X velocity [m/s]"
        if qty == "y":
            data    = np.reshape(self.vel[:,1],shapeGrid)
            lblClbr = "Y velocity [m/s]"
        if qty == "norm": 
            # Calculate norm
            norme1  = np.sqrt(self.vel[:,0]**2 + self.vel[:,1]**2)
            data    = np.reshape(norme1,shapeGrid)
            lblClbr = "Norm of velocity [m/s]"
        if qty == "angle":
            # Calculate angle
            angle1  = np.arctan2(self.vel[:,1],self.vel[:,0])
            data    = np.reshape(angle1,shapeGrid)
            lblClbr = "Angle of velocity [rad]"        
        if qty == "xy":
            # Use HSV colors 
            norm    = np.sqrt(self.vel[:,0]**2 + self.vel[:,1]**2)
            if vmax is None :
                maxNorm = norm[np.isfinite(norm)].max()
            else:
                maxNorm = vmax
            normN   = (norm/maxNorm)*255
            angle   = np.rad2deg(np.arctan2(self.vel[:,1],self.vel[:,0]))/2 + 90
            # Prepare hsv image
            shape   = (shapeGrid[0],shapeGrid[1],3)
            hsv     = np.zeros(shape)
            # Set Saturation to maximum
            hsv[...,1] = 255
            hsv[...,0] = np.reshape(angle, shapeGrid) 
            hsv[...,2] = np.reshape(normN, shapeGrid) 
            
            # Use PIL lib 
            img     = cv.cvtColor(hsv.astype("uint8"), cv.COLOR_HSV2RGB)
            
        # ---- Plot
        ax.imshow(img)
        # Add infos to create the colorbar 
        if qty == "xy":
            #mapp = mpl.cm.ScalarMappable(mpl.colors.Normalize(vmin = -180, vmax = 180), cmap = "hsv")
            #fig.colorbar(mapp,ax=ax,label = "Angle")
            tmp = 1
        else:
            
            if vmin is None and vmax is None :
                mapp = mpl.cm.ScalarMappable(mpl.colors.Normalize(vmin = data[np.isfinite(data)].min(),
                                                                  vmax = data[np.isfinite(data)].max()),
                                             cmap = cmap)
               
            else: 
                mapp = mpl.cm.ScalarMappable(mpl.colors.Normalize(vmin = vmin,vmax = vmax),cmap = cmap)
                # Add colorbar 
            fig.colorbar(mapp,ax=ax,label = lblClbr)
    
    def filter(self):
            """ Remove filtered velocities from list """
            
            f           = np.logical_not(np.isfinite(self.vel))
            self.pos    = np.array([np.delete(self.pos[:,0],f[:,0]),np.delete(self.pos[:,1],f[:,1])]).T
            self.vel    = np.array([np.delete(self.vel[:,0],f[:,0]),np.delete(self.vel[:,1],f[:,1])]).T
            if self.normals != []:
                self.normals = np.array([np.delete(self.normals[:,0],f[:,0]),np.delete(self.normals[:,1],f[:,1])]).T
            
            return
            
    def warpImg(self,
                camera,
                im1,
                im2,
                outRMS = True):  
            """
            Apply velocity field to warp image2 back into position of image1. 
            This tool is used for Backend Error Projection. 
            Output is the warped image and the RMS error computed over the velocity field 
            
            Parameters
            ----------
                - im1 : [npArray]
                    First image (could be ndg or color image)
                - im2 : [npArray]
                    Second image image (could be ndg or color image)
                - outRMS : [bool]
                    If True then the RMS error between im1 and im1_warp is returned. 
                    Error is computed only within the warped area. 
            Returns
            -------
                - warpedImage : [npArray]
                    Image 2 "warped" with the velocity field of image 1  
                - RMS   : [float]
                    RMS error computed over all image 
                - errno : [int]
                    - 1 - Wrong format for inputs 

            """
            #### Checkings 
            if np.shape(im1) != np.shape(im2):
                print("\nERROR: Shape of images mismatch")
                return 1
            # image format -> ndg or color 
            if len(np.shape(im1)) != 2:
                print("\nERROR: Wrong image format")
                return 1
            
            #### Get data
            # shape 
            ny,nx       = np.shape(im1)[:2]
            # Remove nan from arrays
            self.filter()
            # indexes
            tmp         = space2img(self.pos, camera)
            ind         = np.array(tmp,dtype = int)
            # displacement [pixels]
            disp        = camera.velSpace2dispImage(self.vel) 
            # Prepare grid for interpolation
            ij          = np.meshgrid(np.arange(ny), np.arange(nx),indexing = "ij") # indexes of image (/!\ meshgrid gives [0] -> x (j) and [1] -> y (i))
            gridij      = np.array([ij[0],ij[1]])                                   # Create 2D grid containing i and j indexes (stored [i,j] -> [y,x])
            gridij      = np.moveaxis(gridij,0,2)                                   # Reshape the data for compatibility -> [ni,nj,2] instead of [2,ni,nj]
            # Interpolate displacement on all image 
            uv_i        = griddata(ind, disp, gridij, method="cubic")
            # Put velocities in grid and change nan to 0 (for interpolation function)
            u           = np.nan_to_num(uv_i[:,:,1], copy=True, nan=0.0, posinf=None, neginf=None)
            v           = np.nan_to_num(uv_i[:,:,0], copy=True, nan=0.0, posinf=None, neginf=None)
            #### Warp image 
            # Data have to be double for interpolation function
            imout       = np.array(im1,dtype = np.float64)
            imin        = np.array(im2,dtype = np.float64)
            # Use ctypes to wrap C function to Python (much much faster!!)
            lib = ctypes.CDLL('/home/guillaume.bodart/Documents/04_Dev/03_Lib/Image/01_bicubic_c_func/bicubic_interpolation.so')
            # Select the function in the dyn. lib
            bicint = lib.bicubic_interpolation_warp
            # Arrange input and outputs 
            bicint.restype = None
            bicint.argtypes = [ndpointer(ctypes.c_double),
                               ndpointer(ctypes.c_double),
                               ndpointer(ctypes.c_double),
                               ndpointer(ctypes.c_double),
                               ctypes.c_int,
                               ctypes.c_int,
                               ctypes.c_bool]
            try:
                bicint(imin,u,v,imout,nx,ny,True)
            except Exception as e:
                print("\nERROR: Problem while warping image... \n Exception : " + str(e))
                return 2
            # Compute difference between im1 and backprojection 
            # First apply dynamic expansion based on the data within the warped area
            ppout   = dynExpans(imout[np.isfinite(uv_i[:,:,0])])
            pp1     = dynExpans(im1[np.isfinite(uv_i[:,:,0])].astype(float))
            # comput diff
            diff    = ppout - pp1
            RMS       = np.sqrt(np.sum([x**2 for x in diff])/len(diff))
            
            if outRMS:       
                return np.array(np.clip(imout,0,255),dtype = np.uint8),RMS
            else:
                return np.array(np.clip(imout,0,255),dtype = np.uint8)
        
        
class filteredCl(dataCl):
    """
    SyRi - filtered data class:
        
    Contains
    ---------------------
    
        - type [str]
            Where the data came from, typically "particles" or "mesh"
    
        - pos [npArrayFloat]
            X,Y,Z position of particles -- stored as [[index] ,[X,Y,Z]]  
            
        - vel [npArrayFloat]
            Vx,Vy,Vz velocities of particles -- stored as [[index] ,[Vx,Vy,Vz]]
            
        - normals [npArrayFloat]
            Nx,Ny,Nz normal vector of the surface (only for mesh)
            
        - lstFilters [lstStr]
            List containing the filters used 
        
    ---------------------
    """
    
    def __init__(self):
        self.type       = ""
        self.pos        = []
        self.vel        = []
        self.normals    = []
        self.lstFilters = [] 
        
     
        
    def apply(self,data,operation):
         """
         Apply the filter "operation" to the data 

         Parameters
         ----------
            - data [data()]
                Data to be filtered
            - operation : [str]
                Filter to be applied 
                Has to have the shape "[object] [operator] [value]"
                    --> e.g. "X > 0" or "Nz > 0"   

         Returns
         -------
            - errno [int]
                - 0 - No errors
                - 1 - format of 'operation' is wrong
                - 2 - data object is empty 

         """
         buff = operation.split()
         # Security 
         if len(buff)!= 3:
             print("ERROR : format of instruction 'operation' is wrong" )
             return 1 
         if data.isempty():
             print("ERROR : data object is empty")
             return 2 
         # Find which quantity is used for filtering 
         qty = buff[0].lower() # Use for case sensitivity 
             
         if len(qty) == 1:
             # First case : position 
             #-------------------------------------------------------------------
             choice = {"x":0,"y":1,"z":2} # use a dict to convert string to index
             thrsld = float(buff[2])
             try:
                 ind = choice[qty]
             except KeyError:
                 print("ERROR : format of object is wrong. Expected : 'x' or 'y' or 'z'\n Check help of function 'apply' for more info")   
                 return 1
             if buff[1] == ">":                 
                 self.pos       = data.pos[ data.pos[:,ind] > thrsld ].copy()
                 self.vel       = data.vel[ data.pos[:,ind] > thrsld ].copy()
                 self.normals   = data.normals[ data.pos[:,ind] > thrsld ].copy()
                 self.gridSize  = data.gridSize.copy()
                 self.type     = data.type
                 self.upres     = data.upres
             elif buff[1] == "<":
                 self.pos       = data.pos[ data.pos[:,ind] < thrsld ].copy()
                 self.vel       = data.vel[ data.pos[:,ind] < thrsld ].copy()
                 self.normals   = data.normals[ data.pos[:,ind] < thrsld ].copy()
                 self.gridSize  = data.gridSize.copy()
                 self.type     = data.type
                 self.upres     = data.upres
             elif buff[1] == "==":
                 self.pos       = data.pos[ data.pos[:,ind] == thrsld ].copy()
                 self.vel       = data.vel[ data.pos[:,ind] == thrsld ].copy()
                 self.normals   = data.normals[ data.pos[:,ind] == thrsld ].copy()
                 self.gridSize  = data.gridSize.copy()
                 self.type     = data.type
                 self.upres     = data.upres
             else:
                 print("ERROR: operator isn't recognize. Expected : '>' or '<' or '=='.\n Check help of function 'apply' for more info")
                 return 1 
             
         elif len(qty) == 2: 
             if qty[0] == "n":
                 # Second case : normals 
                 #-------------------------------------------------------------------
                 choice = {"nx":0,"ny":1,"nz":2} # use a dict to convert string to index
                 thrsld = float(buff[2])
                 try:
                     ind = choice[qty]
                 except KeyError:
                     print("ERROR : format of object is wrong. Expected : 'nx' or 'ny' or 'nz'\n Check help of function 'apply' for more info")   
                     return 1
                 if buff[1] == ">":                 
                     self.pos       = data.pos[ data.normals[:,ind] > thrsld ].copy()
                     self.vel       = data.vel[ data.normals[:,ind] > thrsld ].copy()
                     self.normals   = data.normals[ data.normals[:,ind] > thrsld ].copy()
                     self.gridSize  = data.gridSize.copy()
                     self.type     = data.type
                     self.upres     = data.upres
                 elif buff[1] == "<":
                     self.pos       = data.pos[ data.normals[:,ind] < thrsld ].copy()
                     self.vel       = data.vel[ data.normals[:,ind] < thrsld ].copy()
                     self.normals   = data.normals[ data.normals[:,ind] < thrsld ].copy()
                     self.gridSize  = data.gridSize.copy()
                     self.type     = data.type
                     self.upres     = data.upres
                 elif buff[1] == "==":
                     self.pos       = data.pos[ data.normals[:,ind] == thrsld ].copy()
                     self.vel       = data.vel[ data.normals[:,ind] == thrsld ].copy()
                     self.normals   = data.normals[ data.normals[:,ind] == thrsld ].copy()
                     self.gridSize  = data.gridSize.copy()
                     self.type     = data.type
                     self.upres     = data.upres
                 else:
                     print("ERROR: operator isn't recognize. Expected : '>' or '<' or '=='.\n Check help of function 'apply' for more info")
                     return 1 
             elif qty[0] == "v":
                 # Third case : velocity 
                 #-------------------------------------------------------------------
                 choice = {"vx":0,"vy":1,"vz":2} # use a dict to convert string to index
                 thrsld = float(buff[2])
                 try:
                     ind = choice[qty]
                 except KeyError:
                     print("ERROR : format of object is wrong. Expected : 'vx' or 'vy' or 'vz'\n Check help of function 'apply' for more info")   
                     return 1
                 if buff[1] == ">":                 
                     self.pos       = data.pos[ data.vel[:,ind] > thrsld ].copy()
                     self.vel       = data.vel[ data.vel[:,ind] > thrsld ].copy()
                     self.normals   = data.normals[ data.vel[:,ind] > thrsld ].copy()
                     self.gridSize  = data.gridSize.copy()
                     self.type      = data.type
                     self.upres     = data.upres
                 elif buff[1] == "<":
                     self.pos       = data.pos[ data.vel[:,ind] < thrsld ].copy()
                     self.vel       = data.vel[ data.vel[:,ind] < thrsld ].copy()
                     self.normals   = data.normals[ data.vel[:,ind] < thrsld ].copy()
                     self.gridSize  = data.gridSize.copy()
                     self.type      = data.type
                     self.upres     = data.upres
                 elif buff[1] == "==":
                     self.pos       = data.pos[ data.vel[:,ind] == thrsld ].copy()
                     self.vel       = data.vel[ data.vel[:,ind] == thrsld ].copy()
                     self.normals   = data.normals[ data.vel[:,ind] == thrsld ].copy()
                     self.gridSize  = data.gridSize.copy()
                     self.type      = data.type
                     self.upres     = data.upres
                 else:
                     print("ERROR: operator isn't recognize. Expected : '>' or '<' or '=='.\n Check help of function 'apply' for more info")
                     return 1 
             else:
                print("ERROR : format of object is wrong.\n Check help of function 'apply' for more info")   
                return 1
         self.lstFilters.append(operation)
         return 0
     
#######################################
### FUNCTIONS 
#######################################

def image2space(imgIndx,camera,xy0 = np.nan):
    """
    Converts image indexes to world units with information of the camera and xy0. 
    Assume that the input image is already ortho-rectified (i.e. image can be assumed to be a plan XY)

    Parameters
    ----------
        - imgIndx : [npArrayInt]
            Indexes of the images to be transformed. --> shape : [(ind),(i,j)]
            WARNING : image coordinate are expected. 
            Values from Fudaa-LSPIV are usually given in a mathematical coordinate space with (0,0) located down left
            So the i index need to be "invert" --> correctiIndex = imageSize(i) - iIndexFudaa
            
        - camera : [lspiv.transform.camera()]
            Contains the information of the camera 
        - xyz0 : [npArrayFloat]
            Offset (in meters) apply on the values X, Y for transformation

    Returns
    -------
        - SpaceCoord [npArrayFloat]
            X,Y values in meters
        - errno [int]
            In case of an error this function returns an integer 
                - 1 - Information missing on the camera : no resolution and no scaling
                - 2 - Image isn't squarred, problem with scaling...
                - 3 - ResolutionInfo of the camera isn't recognize
                - 4 - Shape of grid isn't correct'
    """
    
    if camera.resolutionInfo == "":
        # Case no resolution is entered, use simple scaling (typical blender case with othographic camera)
        if camera.scale == -99:
            print("ERROR: Information missing on the camera : no resolution and no scaling")
            return 1
        # Image size is assumed to be squared 
        if camera.imagesize[0]!=camera.imagesize[1]:
            print("ERROR: Image isn't squarred, problem with scaling...")
            return 1
        res = camera.imagesize[0] / camera.scale 
    elif camera.resolutionInfo == "m/pix":
        # Case resolution is given in meter/pixels (typical units from Fudaa-LSPIV project) 
        res = 1/camera.resolution 
    elif camera.resolutionInfo == "pix/m":
        # Case resolution is given in pixels/meter  
        res = camera.resolution 
    else: 
        print("ERROR: resolution information of the camera isn't recognize. Expected : 'm/pix' or 'pix/m'")
        return 3
    # Checkings 
    if len(np.shape(imgIndx)) > 2:
        print("ERROR: Shape of grid isn't correct. Expected : [;,2]")
        return 4
    # If no xy0 are passed to the function they're calculated from the camera data
    if np.isnan(xy0):
        xy0 = (camera.position - (0.5*camera.scale))[:2]
       
    if len(imgIndx)==0:
        print("WARNING: imgIndx is empty...")
    # Swap column to get (j,i) as j is x and i is y and inverse i ax to be in mathematical coordinate space    
    JIimgIndx = np.array([imgIndx[:,1],camera.imagesize[0] - imgIndx[:,0]]).T    
    out = (JIimgIndx/res) + xy0 
    
    return out
   
def space2img(spaceIndx,camera,xy0 = np.nan):
    """
    Converts image indexes to world units with information of the camera and xy0. 
    Assume that the input image is already ortho-rectified (i.e. image can be assumed to be a plan XY)

    Parameters
    ----------
        - imgIndx : [npArrayInt]
            Indexes of the images to be transformed. --> shape : [(ind),(i,j)]
            WARNING : image coordinate are expected. 
            Values from Fudaa-LSPIV are usually given in a mathematical coordinate space with (0,0) located down left
            So the i index need to be "invert" --> correctiIndex = imageSize(i) - iIndexFudaa
            
        - camera : [lspiv.transform.camera()]
            Contains the information of the camera 
        - xyz0 : [npArrayFloat]
            Offset (in meters) apply on the values X, Y for transformation

    Returns
    -------
        - SpaceCoord [npArrayFloat]
            X,Y values in meters
        - errno [int]
            In case of an error this function returns an integer 
                - 1 - Information missing on the camera : no resolution and no scaling
                - 2 - Image isn't squarred, problem with scaling...
                - 3 - ResolutionInfo of the camera isn't recognize
    """
    if camera.resolutionInfo == "":
        # Case no resolution is entered, use simple scaling (typical blender case with othographic camera)
        if camera.scale == -99:
            print("ERROR : Information missing on the camera : no resolution and no scaling")
            return 1
        # Image size is assumed to be squared 
        if camera.imagesize[0]!=camera.imagesize[1]:
            print("ERROR : Image isn't squarred, problem with scaling...")
            return 1
        res = camera.imagesize[0] / camera.scale 
    elif camera.resolutionInfo == "m/pix":
        # Case resolution is given in meter/pixels (typical units from Fudaa-LSPIV project) 
        res = 1/camera.resolution 
    elif camera.resolutionInfo == "pix/m":
        # Case resolution is given in pixels/meter  
        res = camera.resolution 
    else: 
        print("ERROR : resolution information of the camera isn't recognize. Expected : 'm/pix' or 'pix/m'")
        return 3
    
    # If no xy0 are passed to the function they're calculated from the camera data
    if np.isnan(xy0):
        xy0 = (camera.position - (0.5*camera.scale))[:2]
       
    if len(spaceIndx)==0:
        print("WARNING : imgIndx is empty...")
    # Swap column to get (x,y) as j is x and i is y     
    JIimgIndx = (spaceIndx - xy0) * res # Because here the resolution is in pix/meters
    out = np.array([camera.imagesize[0]-JIimgIndx[:,1],JIimgIndx[:,0]]).T
    
    return out

def dynExpans(val):
    mx, mn = val.max(),val.min()
    return 255*((val-mn)/(mx-mn))

# def velSpace2dispImage(spaceVel,camera,xy0 = np.nan):
#     """
#     Converts image indexes to world units with information of the camera and xy0. 
#     Assume that the input image is already ortho-rectified (i.e. image can be assumed to be a plan XY)

#     Parameters
#     ----------
#         - imgIndx : [npArrayInt]
#             Indexes of the images to be transformed. --> shape : [(ind),(i,j)]
#             WARNING : image coordinate are expected. 
#             Values from Fudaa-LSPIV are usually given in a mathematical coordinate space with (0,0) located down left
#             So the i index need to be "invert" --> correctiIndex = imageSize(i) - iIndexFudaa
            
#         - camera : [lspiv.transform.camera()]
#             Contains the information of the camera 
#         - xyz0 : [npArrayFloat]
#             Offset (in meters) apply on the values X, Y for transformation

#     Returns
#     -------
#         - SpaceCoord [npArrayFloat]
#             X,Y values in meters
#         - errno [int]
#             In case of an error this function returns an integer 
#                 - 1 - Information missing on the camera : no resolution and no scaling
#                 - 2 - Image isn't squarred, problem with scaling...
#                 - 3 - ResolutionInfo of the camera isn't recognize
#     """
#     # if isinstance(camera,transform.camera):
#     #     print("\nERROR : wrong format of camera passed to the function. Expected : lspiv.transform.camera ")
#     #     return 1
#     if camera.resolutionInfo == "":
#         # Case no resolution is entered, use simple scaling (typical blender case with othographic camera)
#         if camera.scale == -99:
#             print("\nERROR : Information missing on the camera : no resolution and no scaling")
#             return 1
#         # Image size is assumed to be squared 
#         if camera.imagesize[0]!=camera.imagesize[1]:
#             print("\nERROR : Image isn't squarred, problem with scaling...")
#             return 1
#         res = camera.imagesize[0] / camera.scale 
#     elif camera.resolutionInfo == "m/pix":
#         # Case resolution is given in meter/pixels (typical units from Fudaa-LSPIV project) 
#         res = 1/camera.resolution 
#     elif camera.resolutionInfo == "pix/m":
#         # Case resolution is given in pixels/meter  
#         res = camera.resolution 
#     else: 
#         print("\nERROR : resolution information of the camera isn't recognize. Expected : 'm/pix' or 'pix/m'")
#         return 3
    
#     # If no xy0 are passed to the function they're calculated from the camera data
#     if np.isnan(xy0):
#         xy0 = (camera.position - (0.5*camera.scale))[:2]
       
#     if len(spaceVel)==0:
#         print("\nWARNING : Values are empty...")
    
#     # Compute displacement based on camera info 
#     XYdisp = spaceVel / camera.fps
#     JIspaceV = (XYdisp)*res  
#     # Swap column to get (i,j) as x is j and y is i     
#     IJindex = np.array([JIspaceV[:,1],JIspaceV[:,0]]).T
    
#     return IJindex
   


#def compareDataClList(lstDataClRef,lstDataClComp,qty = "norm",method = "")
        
            
        
    